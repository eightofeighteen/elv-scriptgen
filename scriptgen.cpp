/*  ScriptGen - Script Generator to Collect Microsoft Windows Event Logs.
 *  Copyright (c) 2013 Bytes Technology Group (Pty) Limited.
 */

#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <cassert>
#include <fstream>
#include <vector>
using namespace std;

/**
 * @brief readFile
 *  Reads a file to a string.  If the file cannot be read, the function asserts.
 * @param filename  string containing the filename to be read.
 * @return  Returns a string containing the contents of the file.
 */
string readFile(string filename) {
	string out = "";
	ifstream fin(filename.c_str());
	if (!fin.good())	assert(0);
	while (!fin.eof()) {
		char ch = fin.get();
		out += ch;
	}
	return out;
}

/**
 * @brief strrepl
 *  Replaces all occurances of a substring with another string in a string.
 * @param in    Source string.
 * @param match Substring to be removed.
 * @param repl  Substring to be inserted.
 * @return      Returns a string with the substitutions.
 */
string strrepl(string in, string match, string repl) {
	string out = in;
	while (true) {
		unsigned int pos = out.find(match);
		if (pos == string::npos)	break;
		out.replace(pos, match.length(), repl);
	}
	return out;
}

/**
 * @brief clearstr
 *  Removes all occurances of a specific character in a string.
 * @param in    Input string to be searched.
 * @param ch    Integer code of the character to be removed.
 * @return      Returns a string with the character removed.
 */
string clearstr(string in, int ch) {
	string out = in;
	int pos = 0;
	while (true) {
		pos = out.find((char) ch);
		if (pos == string::npos)	break;
		out.erase(pos, 1);
	}
	return out;
}

/**
 * @brief repl
 *  Scans a string and replaces specific tokens with values.
 * @param language  Input string containing tokens.
 * @param line      Unused.
 * @param server    Server replacement.
 * @param table     Table replacement.
 * @param dbserver  Database server replacement.
 * @param database  Database replacement.
 * @return          Returns a string with the appropriate replacements.
 */
string repl(string language, string line, string server, string table, string dbserver, string database) {
	//cout << "Language is\n" << language << endl;
	string out = "";
	const string servertoken = "%H";
	const string eventtoken = "%E";
	const string tabletoken = "%T";
	const string dbstoken = "%S";
	const string dbtoken = "%D";
	vector<string> eventlogs;
	eventlogs.push_back("Application");
	eventlogs.push_back("Security");
	eventlogs.push_back("System");
	out = strrepl(language, servertoken, server);
	out = strrepl(out, tabletoken, table);
	out = strrepl(out, dbstoken, dbserver);
	out = strrepl(out, dbtoken, database);
	//cout << "Out is\n" << out << endl;
	return out;
}

/**
 * @brief tuple
 *  Generates a string containing the appropriate event logs category and replaces specific tokens with values.
 * @param language  Input string containing tokens.
 * @param line      Unused.
 * @param server    Sever replacement.
 * @param table     Table replacement.
 * @param dbserver  Database server replacement.
 * @param database  Database replacement.
 * @return          Returns a string with the appropriate replacements.
 */
string tuple(string language, string line, string server, string table, string dbserver, string database) {
	string out = "";
	const string servertoken = "%H";
	const string eventtoken = "%E";
	const string tabletoken = "%T";
	const string dbstoken = "%S";
	const string dbtoken = "%D";
	vector<string> eventlogs;
	eventlogs.push_back("Application");
	//eventlogs.push_back("Security");
	eventlogs.push_back("System");
	for (unsigned int i = 0; i < eventlogs.size(); i++) {
		out += strrepl(language, eventtoken, eventlogs[i]);
	}
	out = strrepl(out, servertoken, server);
	out = strrepl(out, tabletoken, table);
	out = strrepl(out, dbstoken, dbserver);
	out = strrepl(out, dbtoken, database);
	return out;
}

/**
 * @brief writetofile
 *  Writes the contents of a string to a file.
 * @param filename  Filename to be written to.
 * @param contents  String to write to the file.
 */
void writetofile(string filename, string contents) {
	ofstream fout(filename.c_str());
	if (!fout.good())	assert(0);
	fout << contents;
	fout.close();
}

/**
 * @brief uint2str
 *  Converts a unsigned integer to a string.
 * @param value Unsigned integer to be converted.
 * @return      Returns a string containing the unsigned integer.
 */
string uint2str(unsigned int value) {
	string out = "";
	stringstream strm;
	strm << value;
	out = strm.str();
	if (out.length() == 1)	out = "0" + out;
	return out;
}

/**
 * @brief readfromfile
 *  Reads the contents of a file to a string.
 *  If the file cannot be read, the function asserts.
 * @param filename  Filename to be read.
 * @return          Returns a string containing the contents of the file.
 */
string readfromfile(string filename) {
	//cout << "Reading from " << filename << "." << endl;
	string out = "";
	vector<string> fc;
	ifstream fin(filename.c_str());
	if (!fin.good())	assert(0);
	string line = "";
	while (!fin.eof() && fin.good()) {
		getline(fin, line, '\n');
		fc.push_back(line);
	}
	fin.close();
	//cout << "Read " << fc.size() << " lines." << endl;
	for (unsigned int i = 0; i < fc.size(); i++)
		out = out + fc[i] + "\n";
	return out;
}

/**
 * @brief process
 *  Reads various input files and generated various script files to gather event logs.
 *  Generates a wrapper.cmd script that calls a number of gather scripts then executes a SQL script.
 * @param argc  Number of command-line arguments.  Not used.
 * @param argv  List of command-line arguments.  Not used.
 */
void process(int argc, _TCHAR* argv[]) {
	const int STREAMS = 8;
	const int DLIMIT = 512;
	const string table = "dbo.tempeventLogs";
	const string dbserver = "hosql03";
	const string database = "tempdb";
	string languagefn = "language.txt";
	string language = readFile(languagefn);
	//cout << language << endl;
	string filename = "list.txt";
	ifstream fin(filename.c_str());
	if (!fin.good())	assert(0);
	unsigned int count = 0;
	string output = "";
	vector<string> serverlist;
	while (!fin.eof() && count < DLIMIT) {
		count++;
		string line = "";
		fin >> line;
		serverlist.push_back(line);
	}
	vector<vector<string> > out;
	vector<string> outdata;
	for (unsigned int i = 0; i < STREAMS; i++) {
		vector<string> temp;
		out.push_back(temp);
	}
	for (unsigned int i = 0; i < serverlist.size(); i++)
		out[i % STREAMS].push_back(clearstr(tuple(language, serverlist[i], serverlist[i], table, dbserver, database), -1) + "\n");
	fin.close();
	output = "";
	for (unsigned int i = 0; i < STREAMS; i++) {
		for (unsigned int j = 0; j < out[i].size(); j++) {
			output += out[i][j];
		}
		outdata.push_back(output);
		output = "";
	}

	//string pre = "prepend this info\n";
	//string pre = "sqlcmd -i temptables";
	//string post = "sqlcmd -i move_and_clear_stream";
	string pre = "echo BUSY > stream";
	string post = "del stream";
	string sqlcontents = readfromfile(string("sqllanguage.txt"));
	string ttcontents = readfromfile(string("ttlanguage.txt"));
	writetofile(string("temptables.sql"), repl(ttcontents, "", "", table, dbserver, database));
	//cout << sqlcontents << endl;
	//sqlcontents = tuple(sqlcontents, "", "", table + uint2str(55), dbserver, database);

	for (unsigned int i = 0; i < outdata.size(); i++) {
		outdata[i] = "@echo off\n" + pre + uint2str(i + 1) + outdata[i] + post + uint2str(i + 1) + "\nexit\n";
		writetofile(string("gather") + uint2str(i + 1) + string(".cmd"), outdata[i]);
		//cout << string("gather") + uint2str(i + 1) + ".cmd" << endl;
		writetofile("move_and_clear_stream" + uint2str(i + 1) + ".sql", repl(sqlcontents, "", "", table, dbserver, database));
		//writetofile("temptables" + ".sql", repl(ttcontents, "", "", table, dbserver, database));
	}

	string wrapper = "";
	for (unsigned int i = 0; i < STREAMS; i++) {
		wrapper = wrapper + "START gather" + uint2str(i + 1) + ".cmd\n";
	}
	wrapper = "@echo off\nsqlcmd -Q \"USE eventlogArchive; INSERT INTO dbo.timer (comment, timestamp) VALUES ('ELA', GETDATE());\"\nsqlcmd -i temptables.sql\n" + wrapper;
	wrapper += readfromfile("wrapper-end.txt");
	wrapper += "sqlcmd -Q \"USE eventlogArchive; INSERT INTO dbo.timer (comment, timestamp) VALUES ('ELA', GETDATE());\"\necho on\n";
	//cout << wrapper;
	writetofile("wrapper.cmd", wrapper);
}

void showCopyright() {
	vector<string> warning;
	warning.push_back("ScriptGen - Script Generator to Collect Microsoft Windows Event Logs");
	warning.push_back("Version 1.0	-	11 October 2013");
	warning.push_back("Copyright (c) 2013 Bytes Technology Group (Pty) Limited");
	for (unsigned int i = 0; i < warning.size(); i++)
		cout << warning[i] << endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	showCopyright();
	process(argc, argv);
	string line;
	//cin >> line;
	cout << "Generation complete." << endl;
	return 0;
}

